package net.ashyon.emojikeyboard;

import dorkbox.systemTray.MenuEntry;
import dorkbox.systemTray.SystemTray;
import dorkbox.systemTray.SystemTrayMenuAction;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void stop() throws Exception {
        // Save settings on quit
        Settings.saveSettings();
    }

    @Override
    public void start(Stage primaryStage) {
        GridPane rootPane = null;
        try {
            rootPane = new FXMLLoader(getClass().getResource("/layout/main.fxml")).load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Create scene
        Scene scene = new Scene(rootPane);
        // Window title
        primaryStage.setTitle("Emoji Keyboard");
        // Set the scene, light the candles, play the sexy music
        primaryStage.setScene(scene);
        //Set minimum window dimensions
        primaryStage.setMinWidth(400);
        primaryStage.setMinHeight(250);
        // Launch window
        primaryStage.show();

        // Don't destroy the window when closed
        primaryStage.setOnCloseRequest(windowEvent -> {
            primaryStage.hide();
        });


        // Don't exit JavaFX thread when window is hidden
        Platform.setImplicitExit(false);

        // Force GTK2 so it doesn't crash on Linux (JavaFX conflicts with GTK3)
        SystemTray.FORCE_GTK2 = true;

        // Get system tray and set icon
        SystemTray systemTray = SystemTray.getSystemTray();
        systemTray.setIcon(getClass().getResource("/images/tray-icon.png"));


        // Create menu entries
        // TODO: Add submenu for favorites once SystemTray 3.0 is available

        systemTray.addMenuEntry("Emoji Keyboard", new SystemTrayMenuAction() {
            @Override
            public void onClick(SystemTray systemTray, MenuEntry menuEntry) {
                // Show the window when entry is clicked
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        primaryStage.show();
                    }
                });
            }
        });

        systemTray.addMenuEntry("Quit", new SystemTrayMenuAction() {
            @Override
            public void onClick(SystemTray systemTray, MenuEntry menuEntry) {
                // Exit application when entry is clicked
                Platform.exit();
                systemTray.shutdown();
            }
        });
    }
}
