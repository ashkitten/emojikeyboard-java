package net.ashyon.emojikeyboard;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import javafx.stage.Window;
import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.apache.commons.lang3.text.WordUtils;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Util {
    public static final String EMOJI_PATH_LARGE = "/emojione/assets/png_512x512/";
    public static final String EMOJI_PATH_SMALL = "/emojione/assets/png/";

    public static HashMap<String, ArrayList<Emoji>> emojiCategories;
    public static BidiMap emojiMap = new DualHashBidiMap();
    public static ArrayList<Emoji> emojis;

    public static void initEmojiData() {
        // Initialize emoji datasets
        emojis = new ArrayList<>();
        emojiCategories = new HashMap<>();

        // Deserialize JSON
        Gson gson = new Gson();
        emojiMap.putAll(gson.fromJson(
                new InputStreamReader(Util.class.getResourceAsStream("/emojione/emoji.json")),
                new TypeToken<HashMap<String, Emoji>>() {
                }.getType()));

        emojis.addAll(emojiMap.values());
        Collections.sort(emojis, (o1, o2) -> o1.unicode.compareTo(o2.unicode));

        // Sort emoji by category
        for (Emoji emoji : emojis) {
            emoji.category = WordUtils.capitalize(emoji.category);
            if (!emojiCategories.containsKey(emoji.category)) {
                emojiCategories.put(emoji.category, new ArrayList<>());
            }
            emojiCategories.get(emoji.category).add(emoji);
        }

        emojiCategories.remove("Modifier");
    }

    // Utility method for moving windows to the center of other windows
    public static void windowToCenter(Window w1, Window w2) {
        w1.setX(w2.getX() + w2.getWidth() / 2 - w1.getWidth() / 2);
        w1.setY(w2.getY() + w2.getHeight() / 2 - w1.getHeight() / 2);
    }
}
