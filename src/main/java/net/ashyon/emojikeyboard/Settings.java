package net.ashyon.emojikeyboard;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import net.ashyon.emojikeyboard.elements.EmojiButton;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.prefs.Preferences;

public class Settings {
    public static Preferences preferences;

    public static DoubleProperty emojiSize;
    public static BooleanProperty fuzzySearch;
    public static ObservableList<Node> favorites;

    static {
        // Initialize stuff
        preferences = Preferences.userNodeForPackage(Settings.class);

        // Second argument is defaults
        emojiSize = new SimpleDoubleProperty(preferences.getDouble("emojiSize", 32.0));
        fuzzySearch = new SimpleBooleanProperty(preferences.getBoolean("fuzzySearch", false));
    }

    static void saveSettings() {
        // Save settings
        preferences.putDouble("emojiSize", emojiSize.get());
        preferences.putBoolean("fuzzySearch", fuzzySearch.get());

        // Save favorites
        ArrayList<String> favoritesStrings = new ArrayList<>();
        for (Node node : favorites) {
            assert node instanceof EmojiButton;
            favoritesStrings.add((String) Util.emojiMap.inverseBidiMap().get(((EmojiButton) node).getEmoji()));
        }
        preferences.put("favorites", StringUtils.join(favoritesStrings, ","));
    }

    public static boolean isFavorite(Emoji emoji) {
        return favorites.contains(new EmojiButton(emoji));
    }

    public static void addFavorite(Emoji emoji) {
        favorites.add(new EmojiButton(emoji));
    }

    public static void removeFavorite(Emoji emoji) {
        favorites.remove(new EmojiButton(emoji));
    }

}
