package net.ashyon.emojikeyboard.elements;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import net.ashyon.emojikeyboard.Emoji;
import net.ashyon.emojikeyboard.Settings;
import net.ashyon.emojikeyboard.Util;
import net.ashyon.emojikeyboard.layout.PopupLayoutController;

import java.io.IOException;

public class EmojiButton extends Button {
    private final Emoji emoji;

    public EmojiButton(Emoji emoji) {
        this.emoji = emoji;

        ImageView imageView = new ImageView(new Image(getClass().getResourceAsStream(Util.EMOJI_PATH_SMALL + emoji.unicode + ".png")));
        imageView.fitWidthProperty().bind(Settings.emojiSize);
        imageView.fitHeightProperty().bind(Settings.emojiSize);

        setGraphic(imageView);
    }

    @Override
    public void fire() {
        GridPane layoutGrid = null;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/popup.fxml"));
            layoutGrid = loader.load();
            ((PopupLayoutController) loader.getController()).init(emoji);
        } catch (IOException e) {
            e.printStackTrace();
        }

        final Scene scene = new Scene(layoutGrid);

        Stage stage = new Stage();
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL); // Keep on top

        stage.setResizable(false);
        stage.show();

        // Variables for the main and popup windows
        Window popupWindow = scene.getWindow();
        Window mainWindow = this.getScene().getWindow();

        // Move popup window to the middle of the parent
        Util.windowToCenter(popupWindow, mainWindow);

        // Close window on ESC key
        scene.setOnKeyPressed(keyEvent -> {
            if (keyEvent.getCode().equals(KeyCode.ESCAPE)) stage.close();
        });

        scene.getRoot().requestFocus();
    }

    public Emoji getEmoji() {
        return emoji;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass().equals(EmojiButton.class)) {
            EmojiButton other = (EmojiButton) obj;
            return other.getEmoji().equals(this.emoji);
        }
        return false;
    }
}
