// Deserialization class for emoji JSON

package net.ashyon.emojikeyboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class Emoji {
    @SerializedName("unicode")
    @Expose
    public String unicode;

    @SerializedName("unicode_alternates")
    @Expose
    public String unicodeAlternates;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("shortname")
    @Expose
    public String shortname;

    @SerializedName("category")
    @Expose
    public String category;

    @SerializedName("emoji_order")
    @Expose
    public String emojiOrder;

    @SerializedName("aliases")
    @Expose
    public List<String> aliases = new ArrayList<>();

    @SerializedName("aliases_ascii")
    @Expose
    public List<String> aliasesAscii = new ArrayList<>();

    @SerializedName("keywords")
    @Expose
    public List<String> keywords = new ArrayList<>();

    public Emoji(String name) {
        this((Emoji) Util.emojiMap.get(name));
    }

    public Emoji(Emoji other) {
        this.unicode = other.unicode;
        this.unicodeAlternates = other.unicodeAlternates;
        this.name = other.name;
        this.shortname = other.shortname;
        this.category = other.category;
        this.emojiOrder = other.emojiOrder;
        this.aliases = other.aliases;
        this.aliasesAscii = other.aliasesAscii;
        this.keywords = other.keywords;
    }
}
