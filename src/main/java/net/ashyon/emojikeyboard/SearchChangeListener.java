package net.ashyon.emojikeyboard;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.FlowPane;
import net.ashyon.emojikeyboard.elements.EmojiButton;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.analysis.ngram.NGramTokenizer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.*;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SearchChangeListener implements ChangeListener<String> {
    private final FlowPane resultPane;
    private final ScrollPane scrollWrapper;
    private HashMap<String, EmojiButton> emojiButtonMap;
    private Directory index;


    public SearchChangeListener(FlowPane resultPane, ScrollPane scrollWrapper) {
        // Init variables
        this.resultPane = resultPane;
        this.scrollWrapper = scrollWrapper;

        try {
            // Create index and analyzer
            StandardAnalyzer analyzer = new StandardAnalyzer();
            index = new RAMDirectory();
            IndexWriterConfig config = new IndexWriterConfig(analyzer);
            IndexWriter indexWriter = new IndexWriter(index, config);

            // Add emojis to index
            this.emojiButtonMap = new HashMap<>();
            for (Emoji emoji : Util.emojis) {
                emojiButtonMap.put(emoji.name, new EmojiButton(emoji));
                addDoc(indexWriter, emoji.name, emoji.keywords);
            }
            indexWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void addDoc(IndexWriter w, String name, List<String> keywords) throws IOException {
        Document document = new Document();

        // Add normal fields
        document.add(new TextField("name", name, Field.Store.YES));
        document.add(new TextField("keywords", StringUtils.join(keywords, " "), Field.Store.YES));

        // Add n-grams
        ArrayList<String> ngrams = new ArrayList<>();

        // Create n-grams from name
        StringReader stringReader = new StringReader(name);

        // Set min/max n-grams
        NGramTokenizer tokenizer = new NGramTokenizer(1, name.length());

        // Tokenize n-grams
        tokenizer.setReader(stringReader);
        CharTermAttribute charTermAttribute = tokenizer.addAttribute(CharTermAttribute.class);
        tokenizer.reset();

        // Read tokens to string
        while (tokenizer.incrementToken()) {
            String token = charTermAttribute.toString();
            ngrams.add(token);
        }

        // Close tokenizer
        tokenizer.end();
        tokenizer.close();

        // Add n-gram field to document
        document.add(new TextField("ngrams", StringUtils.join(ngrams, " "), Field.Store.YES));

        // Add document to index
        w.addDocument(document);
    }

    @Override
    public void changed(
            ObservableValue<? extends String> observableValue, String oldText, String currentText) {
        // Only do search if there is something to search
        if (currentText.length() == 0) {
            scrollWrapper.setVisible(false);
        } else {
            // Set search results pane visible
            scrollWrapper.setVisible(true);

            // Clear result pane
            resultPane.getChildren().clear();

            try {
                // Init index reader
                IndexReader indexReader = DirectoryReader.open(index);
                IndexSearcher indexSearcher = new IndexSearcher(indexReader);

                // Init boolean query builder
                BooleanQuery.Builder builder = new BooleanQuery.Builder();

                // Add queries for search
                // Fuzzy search optional
                if (Settings.fuzzySearch.get()) {
                    builder.add(new FuzzyQuery(new Term("keywords", currentText)), BooleanClause.Occur.SHOULD);
                    builder.add(new FuzzyQuery(new Term("name", currentText)), BooleanClause.Occur.SHOULD);
                    builder.add(new FuzzyQuery(new Term("ngrams", currentText)), BooleanClause.Occur.SHOULD);
                } else {
                    builder.add(new TermQuery(new Term("keywords", currentText)), BooleanClause.Occur.SHOULD);
                    builder.add(new TermQuery(new Term("name", currentText)), BooleanClause.Occur.SHOULD);
                    builder.add(new TermQuery(new Term("ngrams", currentText)), BooleanClause.Occur.SHOULD);
                }

                // Get top hits from search
                TopDocs topDocs = indexSearcher.search(builder.build(), 1000, Sort.RELEVANCE);
                ScoreDoc[] hits = topDocs.scoreDocs;
                for (ScoreDoc hit : hits) {
                    EmojiButton button = emojiButtonMap.get(indexSearcher.doc(hit.doc).get("name"));

                    // Add results to the pane
                    if (!resultPane.getChildren().contains(button)) {
                        resultPane.getChildren().add(button);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
