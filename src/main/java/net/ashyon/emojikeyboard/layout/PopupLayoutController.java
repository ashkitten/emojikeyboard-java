package net.ashyon.emojikeyboard.layout;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import net.ashyon.emojikeyboard.Emoji;
import net.ashyon.emojikeyboard.Settings;
import net.ashyon.emojikeyboard.Util;
import org.apache.commons.lang3.text.WordUtils;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

public class PopupLayoutController {
    @FXML
    private GridPane rootPane;
    @FXML
    private Label emojiName;
    @FXML
    private Label emojiUnicode;
    @FXML
    private ImageView emojiLarge;
    @FXML
    private Button copyButton;
    @FXML
    private Button favoriteButton;

    public void init(Emoji emoji) {
        emojiName.setText(WordUtils.capitalize(emoji.name));
        emojiUnicode.setText("U+" + emoji.unicode.toUpperCase());
        emojiLarge.setImage(new Image(getClass().getResourceAsStream(Util.EMOJI_PATH_LARGE + emoji.unicode + ".png")));

        copyButton.setOnAction(actienEvent -> {
            String selection = "";

            for (String codepoint : emoji.unicode.split("-")) {
                char[] character = Character.toChars(Integer.parseInt(codepoint, 16));
                selection += new String(character);
            }

            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            StringSelection stringSelection = new StringSelection(selection);
            clipboard.setContents(stringSelection, null);

            copyButton.setText("Copied");
            copyButton.getStyleClass().add("copied");

        });

        if (Settings.isFavorite(emoji)) favoriteButton.getStyleClass().add("favorited");
        favoriteButton.setOnAction(actionEvent -> {
            if (!Settings.isFavorite(emoji)) {
                Settings.addFavorite(emoji);
                favoriteButton.getStyleClass().add("favorited");
            } else {
                Settings.removeFavorite(emoji);
                favoriteButton.getStyleClass().remove("favorited");
            }
        });
    }
}
