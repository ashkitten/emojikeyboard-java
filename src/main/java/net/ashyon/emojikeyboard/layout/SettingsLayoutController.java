package net.ashyon.emojikeyboard.layout;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;
import net.ashyon.emojikeyboard.Settings;

public class SettingsLayoutController {
    @FXML
    private TextField emojiSizeSetting;
    @FXML
    private Button emojiSizeSettingIncrement;
    @FXML
    private Button emojiSizeSettingDecrement;
    @FXML
    private CheckBox fuzzySearchSetting;

    public void initialize() {
        emojiSizeSetting.textProperty().bindBidirectional(Settings.emojiSize, new NumberStringConverter());

        emojiSizeSettingIncrement.setOnAction((actionEvent -> {
            emojiSizeSetting.setText(Double.toString(Double.parseDouble(emojiSizeSetting.getText()) + 1));
        }));

        emojiSizeSettingDecrement.setOnAction((actionEvent -> {
            emojiSizeSetting.setText(Double.toString(Double.parseDouble(emojiSizeSetting.getText()) - 1));
        }));

        fuzzySearchSetting.selectedProperty().bindBidirectional(Settings.fuzzySearch);
    }
}
