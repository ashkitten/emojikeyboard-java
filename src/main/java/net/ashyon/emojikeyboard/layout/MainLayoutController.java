package net.ashyon.emojikeyboard.layout;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import net.ashyon.emojikeyboard.Emoji;
import net.ashyon.emojikeyboard.SearchChangeListener;
import net.ashyon.emojikeyboard.Settings;
import net.ashyon.emojikeyboard.Util;
import net.ashyon.emojikeyboard.elements.EmojiButton;

import java.io.IOException;

import static net.ashyon.emojikeyboard.Settings.favorites;

public class MainLayoutController {
    public static FlowPane favoritesPane;
    @FXML
    private GridPane rootPane;
    @FXML
    private TextField searchBox;
    @FXML
    private TabPane categoryTabPane;
    @FXML
    private ScrollPane searchResultScrollWrapper;
    @FXML
    private FlowPane searchResultPane;
    @FXML
    private Button settingsButton;

    public void initialize() {
        searchResultPane.prefWidthProperty().bind(rootPane.widthProperty());

        Util.initEmojiData();

        favoritesPane = new FlowPane(8, 8);
        favoritesPane.setPadding(new Insets(10));
        favoritesPane.prefWidthProperty().bind(rootPane.widthProperty());

        ScrollPane favoritesScrollPane = new ScrollPane(favoritesPane);
        favoritesScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        Tab favoritesTab = new Tab("Favorites", favoritesScrollPane);
        categoryTabPane.getTabs().add(favoritesTab);

        favorites = favoritesPane.getChildren();

        for (String string : Settings.preferences.get("favorites", "").split(",")) {
            if (Util.emojiMap.containsKey(string)) {
                favorites.add(new EmojiButton((Emoji) Util.emojiMap.get(string)));
            }
        }

        // Add emoji to tabs
        for (String category : Util.emojiCategories.keySet()) {
            // Init emoji pane
            FlowPane emojiPane = new FlowPane(8, 8);
            emojiPane.setPadding(new Insets(10));
            emojiPane.prefWidthProperty().bind(rootPane.widthProperty());

            // Init scrollpane wrapper (for scrolling, duh)
            ScrollPane scrollPane = new ScrollPane(emojiPane);
            scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER); // Disable horizontal scrollbar

            // Add to tab pane
            Tab tab = new Tab(category, scrollPane);
            categoryTabPane.getTabs().add(tab);

            // Add all emojis in the category
            for (Emoji emoji : Util.emojiCategories.get(category)) {
                EmojiButton button = new EmojiButton(emoji);
                emojiPane.getChildren().add(button);
            }
        }

        // Create net.ashyon.emojikeyboard.SearchChangeListener
        SearchChangeListener searchChangeListener = new SearchChangeListener(searchResultPane, searchResultScrollWrapper);
        searchBox.textProperty().addListener(searchChangeListener);

        // Set search bar selected
        searchBox.requestFocus();
    }

    @FXML
    private void onSettingsButtonAction(ActionEvent actionEvent) throws IOException {
        // Load settings FXML
        Parent parent = FXMLLoader.load(getClass().getResource("/layout/settings.fxml"));
        // Create stuff
        Scene scene = new Scene(parent);
        Stage stage = new Stage();
        // Set scene
        stage.setScene(scene);
        // Set title
        stage.setTitle("Settings");
        // Stay on top
        stage.initModality(Modality.APPLICATION_MODAL);
        // Show settings window
        stage.show();

        // Move window to center of parent
        Util.windowToCenter(stage, MainLayoutController.this.rootPane.getScene().getWindow());
    }
}
